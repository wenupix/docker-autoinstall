# Docker CE autoinstall script

Script for install Docker CE. Supported Archlinux, Debian/Ubuntu, Centos and Alpine Linux (devel).

## Use (except Alpine Linux)

To use, simply run:
```bash
# ./docker-ce_install.sh
```

## For Alpine Linux users

Before to use, first install bash shell.
```bash
# apk update
# apk upgrade
# apk add bash
```

Now, run script:
```bash
# ./docker-ce_install.sh
```

## NOTE
**This script is ONLY for new Docker installation.**

Old Docker installation will be removed. Data in containers could be erased.

Please, back up your data (and Docker persistence volumes).

## Licence

MIT

