#!/bin/bash

if [ $( id -u ) -ne 0 ]; then
    echo ":ERROR: root permission required."
    exit 1
fi
echo "==============================================================================="
echo " DOCKER-CE & DOCKER-COMPOSE INSTALL SCRIPT"
echo " Jonatan Huenupil F. - v0.1a (20200515)"
echo "==============================================================================="

echo "==> Checking system"
LINUX_DISTRO=$(cat /etc/os-release | awk -F= '/^ID=/ { print $2 }' | tr -d '"' )
echo "  -> Distro: '$(cat /etc/os-release | awk -F= '/^PRETTY_NAME=/ { print $2 }' | tr -d '"')'"

if [ "$LINUX_DISTRO" = "alpine" ]; then
    if [ $(command -v bash; echo $?) -ne 0 ]; then
        echo ":WARN: 'bash' shell not installed (is necessary to start this script). "
        echo "==> Installing 'bash'..."
        apk update
        if [ $? -ne 0 ]; then echo ":ERROR: Failed to update. Exit. (cod: $?)"; exit 8; fi
        apk add bash
        if [ $? -ne 0 ]; then echo ":ERROR: Failed to install. Exit. (cod: $?)"; exit 8; fi
        echo -ne "\n\n\n::\n"
        echo ":: Installation of 'bash' complete."
        echo ":: Please, logout and login this session. Then re-run this script."
        exit 0
    fi
fi

error_distro() {
    echo ":ERROR: GNU/Linux distribution not compatible. Exit."
    exit 30
}
#
update_repo() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            apt-get update -qq > /dev/null && sync
            apt-get upgrade -y
            if [ $? -ne 0 ]; then
                echo ":ERROR: System update failed. Exit. (cod: $?)"
                exit 8
            fi
            ;;
        "centos")
            yum update -y &> /dev/null
            if [ $? -ne 0 ]; then
                echo ":ERROR: System update failed. Exit. (cod: $?)"
                exit 8
            fi
            ;;
        "arch")
            pacman -Syuu --noconfirm
            if [ $? -ne 0 ]; then
                echo ":ERROR: System update failed. Exit. (cod: $?)"
                exit 8
            fi
            ;;
        "alpine")
            apk update
            if [ $? -ne 0 ]; then
                echo ":ERROR: System update failed. Exit. (cod: $?)"
                exit 8
            fi
            ;;
        *)
            error_distro
            ;;
    esac
}
#
remove_dockerio() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            apt-get purge docker docker-engine docker.io containerd runc -qq >/dev/null | true
            ;;
        "centos")
            yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
            ;;
        "arch" | "alpine")
            echo ":INFO: No packages for to remove. Continue."
            ;;
        *)
            error_distro
            ;;
    esac
}
install_dockerce_deps() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            ;;
        "centos")
            yum install -y yum-utils overlay2 && sync
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            ;;
        "arch" | "alpine")
            echo ":INFO: No dependencies for install. Continue."
            ;;
        *)
            error_distro
            ;;
    esac
}
add_dockerce_repo() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            echo "  -> Agregando la key GPG oficial de Docker"; sleep 2
            curl -fsSL https://download.docker.com/linux/$LINUX_DISTRO/gpg | apt-key add -
            if [ $? -ne 0 ]; then
                echo ":ERROR: Problema al obtener la key. No es posible continuar."
                exit 3
            fi
            sync
            apt-key fingerprint 0EBFCD88
            if [ $? -ne 0 ]; then
                echo ":ERROR: Problema al importa key. No es posible continuar."
                exit 4
            fi
            echo "  -> Agregando repositorio Docker"; sleep 2
            add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$LINUX_DISTRO $(lsb_release -cs) stable"
            #add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
            if [ $? -ne 0 ]; then
                echo ":ERROR: Problema al agregar el nuevo repositorio. No es posible continuar."
                exit 5
            fi
            echo "  -> Actualizando indices de paquetes"; sleep 2
            update_repo
            ;;
        "centos")
            echo "  -> Agregando repositorio Docker"; sleep 2
            yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
            if [ $? -ne 0 ]; then
                echo ":ERROR: Problema al agregar el nuevo repositorio. No es posible continuar."
                exit 5
            fi
            echo "  -> Actualizando indices de paquetes"; sleep 2
            update_repo
            sleep 2
            ;;
        "arch" | "alpine")
            echo ":INFO: No repositories for to add. Omitting..."
            ;;
        *)
            error_distro
            ;;
    esac
}
install_dockerce() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            apt-get install -y docker-ce docker-ce-cli containerd.io
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            sync
            ;;
        "centos")
            yum install -y docker-ce docker-ce-cli containerd.io
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            sync
            ;;
        "arch")
            pacman -Sy --noconfirm docker containerd runc
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            ;;
        "alpine")
            apk add docker
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            ;;
        *)
            error_distro
            ;;
    esac
}
enable_dockerce_serv() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian" | "centos" | "arch")
            systemctl enable docker
            systemctl start docker
            ;;
        "alpine")
            echo "  -> Enable docker service at boot..."
            rc-update add docker boot
            echo "  -> Start docker service"
            service docker start
            ;;
        *)
            error_distro
            ;;
    esac
}

install_dockercomp() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian")
            apt-get install -y docker-compose
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            sync
            ;;
        "centos")
            echo "  -> Agregando repositorio 'epel-release'" ; sleep 2
            yum install -y epel-release
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            sleep 2
            echo "  -> Instalando dependencias" ; sleep 2
            yum install -y python-pip
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            yes | pip install --upgrade pip
            echo "  -> Instalando 'docker-compose'" ; sleep 2
            curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            chmod +x /usr/local/bin/docker-compose
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la descarga e instalacion de docker-compose. Saliendo. (cod: $?)"
                exit 10
            fi
            ln -sf /usr/local/bin/docker-compose /usr/bin/docker-compose
            ;;
        "arch")
            echo "  -> Instalando dependencias" ; sleep 2
            pacman -Sy --noconfirm python-pip
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            echo "  -> Instalando 'docker-compose'" ; sleep 2
            wget "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -O /usr/local/bin/docker-compose -o /dev/stdout
            chmod +x /usr/local/bin/docker-compose
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la descarga e instalacion de docker-compose. Saliendo. (cod: $?)"
                exit 10
            fi
            ln -sf /usr/local/bin/docker-compose /usr/bin/docker-compose
            ;;
        "alpine")
            echo "  -> Installing docker-compose dependencies..."
            apk add py-pip python3-dev libffi-dev openssl-dev gcc libc-dev make
            if [ $? -ne 0 ]; then
                echo ":ERROR: Fallo la instalacion de paquetes. Saliendo. (cod: $?)"
                exit 9
            fi
            echo "  -> Installing docker-compose..."
            apk add docker-compose
            ;;
        *)
            error_distro
            ;;
    esac
}
add_user_group() {
    case $LINUX_DISTRO in
        "ubuntu" | "debian" | "arch" | "centos")
            groupadd docker &>/dev/null
            if [ $? -eq 9 ]; then
                echo "  -> Grupo ya existe. Continuando..."; sleep 2
            fi
            if [ $? -ne 0 ] && [ $? -ne 9 ]; then
                echo ":WARN: Problema al crear grupo nuevo. (cod. $?)"
                echo ":WARN: Revise y configure manualmente."
            fi
            for item in "${USR_DOCKER[@]}"; do
                echo "  -> Agregando '$item' al grupo 'docker'..."; sleep 2
                usermod -aG docker $item &>/dev/null
                if [ $? -ne 0 ]; then
                    echo ":WARN: Problema al agregar el usuario '$item' al grupo. (cod. $?)"
                    echo ":WARN: Revise y configure manualmente."
                fi
            done
            ;;
        "alpine")
            addgroup -S docker &>/dev/null
            if [ $? -ne 0 ]; then
                echo ":WARN: Problema al crear grupo nuevo. (cod. $?)"
                echo ":WARN: Revise y configure manualmente."
            fi
            for item in "${USR_DOCKER[@]}"; do
                echo "  -> Adding '$item' to 'docker' group..."; sleep 2
                addgroup $item docker &>/dev/null
                if [ $? -ne 0 ]; then
                    echo ":WARN: Problema al agregar el usuario '$item' al grupo. (cod. $?)"
                    echo ":WARN: Revise y configure manualmente."
                fi
            done
            ;;
        *)
            error_distro
            ;;
    esac
}

# obteniendo lista de usuarios
TMP_USR_LIST=$( eval getent passwd {$(awk '/^UID_MIN/ {print $2}' /etc/login.defs)..$(awk '/^UID_MAX/ {print $2}' /etc/login.defs)} | cut -d: -f1 )
OLD_IFS=$IFS
IFS=$'\n'
USR_LIST=($TMP_USR_LIST)
IFS=$OLD_IFS
echo "::: A continuacion se le preguntara el o los usuarios que ejecutaran Docker."
echo "::: Los siguientes usuarios se encuentran en este sistema: '${USR_LIST[@]}' "
echo "::: - Presione ENTER para asignar el usuario principal (${USR_LIST[0]})"
echo "::: - Ingrese los usuarios (separados por un espacio) que usaran Docker. (ejemplo: alice bob jony)"
echo "::: - Ingrese * para aplicar a todos"
echo ":INFO: Luego de esto, se iniciara la instalacion desatendida"

read -p "==< Usuario? (${USR_LIST[0]}): " USR_OPTION

case $USR_OPTION in
    "*")
        USR_DOCKER=(${USR_LIST[@]})
        ;;
    "")
        USR_DOCKER=${USR_LIST[0]}
        ;;
    *)
        USR_DOCKER=($USR_OPTION)
        ;;
esac

echo "==> Verificando usuario(s)"
for item in "${USR_DOCKER[@]}"; do
    #echo "  -> Usuario: '$item' "
    id -u $item &>/dev/null
    if [ $? -ne 0 ]; then
        echo ":ERROR: Usuario '$item' no existe en el sistema. Saliendo."
        exit 9
    fi
done

echo "==> Iniciando la instalacion de 'docker-ce'"
echo "==> Verificando conexion"
curl --fail -sB https://download.docker.com/ > /dev/null
if [ $? -ne 0 ]; then
    echo ":ERROR: Verifique la conexion a internet. (curl: error $?)"
    exit 2
fi
echo "  -> OK!"

echo "==> Actualizando repositorios"; sleep 2
update_repo

echo "==> Eliminando 'docker.io' para instalar la version 'docker-ce'"; sleep 2
remove_dockerio

echo "==> Instalando dependencias"; sleep 2
install_dockerce_deps

echo "==> Agregando nuevo repositorio docker en APT"; sleep 2
add_dockerce_repo

echo "==> Instalando 'docker-ce'"; sleep 2
install_dockerce

echo "==> Post-configuracion"; sleep 2
enable_dockerce_serv

echo "  -> Creando grupo 'docker'..."; sleep 2
add_user_group

echo "  -> Verificando instalacion de Docker"; sleep 2
docker --version
if [ $? -ne 0 ]; then
    echo ":ERROR: Fallo la instalacion de Docker: no se encuentra el comando. Saliendo."
    error 31
fi
echo "::: Instalacion de Docker finalizado."
echo "==> Iniciando instalacion de 'docker-compose'"; sleep 2
install_dockercomp

echo "  -> Verificando instalacion de docker-compose"; sleep 2
docker-compose --version
if [ $? -ne 0 ]; then
    echo ":ERROR: Fallo la instalacion de docker-compose: no se encuentra el comando. Saliendo."
    error 32
fi

echo ":::"
echo "::: Instalacion de Docker-CE y Docker-compose finalizado."
echo "::: Cierre e inicie la sesion para ver los cambios."
echo ":::"
echo "==> LISTO."
